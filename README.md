# UE5 Modified OpenCV plugin to support camera streaming

** WARNING **
This is only quick & dirty proof of concept implementation.

Its not meant to be used in production, licensing issues may apply.

Not also that the end result may be dithered, as there seems to be a bug
in the code that reads from the cv2 Mat.

Camera api is available under "OpenCV" category in blueprints.
